///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>

#include "cat.h"
#include "animals.h"

// @todo declare an array of struct Cat, call it catDB and it should have at least MAX_SPECIES records in it
struct Cat {
   char name[30];
   enum Gender gender;
   enum CatBreeds breed;
   bool isFixed;
   float weight;
   enum Color collar1_color;
   enum Color collar2_color;
   long license;

};

struct Cat catDB[MAX_SPECIES];




/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) {
   strcpy(catDB[i].name, "Alice");
   catDB[i].gender = FEMALE;
   catDB[i].breed = MAIN_COON;
   catDB[i].isFixed = true;
   catDB[i].weight = 12.34;
   catDB[i].collar1_color = BLACK;
   catDB[i].collar2_color = RED;
   catDB[i].license = 12345;

}



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
   // Here's a clue of what one printf() might look like...
   // printf ("    collar color 1 = [%s]\n", colorName( catDB[i].collar1_color ));
   printf("Cat Name: [%s]\n", catDB[i].name);
   printf("Gender: [%s]\n", genderName(catDB[i].gender));
   printf("Breed: [%s]\n", breedName(catDB[i].breed));
   printf("is Fixed: [%s]\n", boolstring(catDB[i].isFixed));
   printf("Weight: [%f]\n", catDB[i].weight);
   printf ("Collar color 1 = [%s]\n", colorName( catDB[i].collar1_color ));
   printf ("Collar color 2 = [%s]\n", colorName( catDB[i].collar2_color ));
   printf("License: [%lu]\n", catDB[i].license);
}

