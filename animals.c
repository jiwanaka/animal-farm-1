///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

char* breedName(enum CatBreeds breed){
   switch(breed){
      case 0:
         return "Main Coon";
      case 1:
         return "Manx";
      case 2:
         return "Shorthair";
      case 3:
         return "Persian";
      case 4:
         return "Sphynx";
      default:
         return "Invalid Species";
   }
}

char* genderName(enum Gender gender){
   switch(gender){
   case 0:
      return "Male";
   case 1:
      return "Female";
   default:
      return "Gender not available";
}
}
/// Decode the enum Color into strings for printf()
// char* colorName (enum Color color) {
char* colorName(enum Color color){
   switch(color){
      case 0:
         return "Black";
      case 1:
         return "White";
      case 2:
         return "Red";
      case 3:
         return "Blue";
      case 4:
         return "Green";
      case 5:
         return "Pink";
      default:
         return "Invalid Color";
   }
   return NULL;
   };
   // @todo Map the enum Color to a string

//   return NULL; // We should never get here
// };

char *boolstring( _Bool b ) { return b ? "True" : "False"; }


